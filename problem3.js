//Find all users with masters Degree.


function getUserOfMasters(users) {
  

  let userOfMasters = [];

  for(let keys in users){
    if(users[keys].qualification === "Masters"){
      userOfMasters.push(keys);
    }
  }

  return userOfMasters;

}

module.exports = getUserOfMasters;