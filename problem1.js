//Q1 Find all users who are interested in playing video games.

function getVideoGamesUser(users) {
  let videoGameUser = [];

  for (let keys in users) {
    let interest = users[keys].interest || users[keys].interests;

    let splitedInterest = interest[0].split(", ");

    for (let index = 0; index < splitedInterest.length; index++) {
      if (splitedInterest[index].includes("Video Games")) {
        videoGameUser.push(keys);
      }
    }
  }

  return videoGameUser;
}

module.exports = getVideoGamesUser;
