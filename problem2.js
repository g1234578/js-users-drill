//Find all users staying in Germany.

function getUserInGermany(users) {
  let userInGermany = [];

  for (let keys in users) {
    if (users[keys].nationality === "Germany") {
      userInGermany.push(keys);
    }
  }

  return userInGermany;
}

 module.exports = getUserInGermany;


