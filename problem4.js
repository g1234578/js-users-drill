//Q4 Group users based on their Programming language mentioned in their designation.

function getGroupsByDesignation(users) {
  const languages = ["C++", "Golang", "Javascript", "Java", "Python", "CSharp"];

  let groupByDesignation = {};

  for (let keys in users) {
    let designation = users[keys].desgination;
    let splitedDesignation = designation.split(" ");

    for (let index = 0; index < splitedDesignation.length; index++) {
      let usersLanaguage = splitedDesignation[index];
      if (languages.includes(usersLanaguage)) {
        if (groupByDesignation[usersLanaguage]) {
          groupByDesignation[usersLanaguage].push(keys);
        } else {
          groupByDesignation[usersLanaguage] = [];
          groupByDesignation[usersLanaguage].push(keys);
        }
      }
    }
  }

  return groupByDesignation;
}

 module.exports = getGroupsByDesignation;
